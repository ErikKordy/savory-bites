from pydantic import BaseModel
from typing import List, Optional
from jwtdown_fastapi.authentication import Token


class ChefIn(BaseModel):
    name: str
    recipe_id: str


class FavoriteIn(BaseModel):
    recipe_id: str


class FavoriteOut(BaseModel):
    id: str
    account_id: str
    recipe_id: str


class Favorites(BaseModel):
    favorites: List[FavoriteOut]


class Error(BaseModel):
    message: str


class Instructions(BaseModel):
    position: int
    display_text: str


class Credits(BaseModel):
    name: str | None


class UserRatings(BaseModel):
    count_positive: Optional[int]
    score: Optional[float]
    count_negative: Optional[int]


class RecipesIds(BaseModel):
    id: str
    name: str
    thumbnail_url: str
    credits: List[Credits]
    user_ratings: UserRatings


class RecipesOut(BaseModel):
    name: str
    description: str
    thumbnail_url: str
    user_ratings: UserRatings
    description: str
    credits: List[Credits]
    instructions: List[Instructions]


class RecipesList(BaseModel):
    recipes: list[RecipesIds]


class AccountIn(BaseModel):
    email: str
    password: str
    first_name: str
    last_name: str


class AccountOut(BaseModel):
    id: str
    email: str
    first_name: str
    last_name: str


class AccountOutWithPassword(AccountOut):
    hashed_password: str
    email: str


class AccountForm(BaseModel):
    username: str
    password: str


class AccountToken(Token):
    account: AccountOut


class HttpError(BaseModel):
    detail: str

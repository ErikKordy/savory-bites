from models import AccountIn, AccountOutWithPassword
from queries.client import MongoQueries


class DuplicateAccountError(ValueError):
    pass


class AccountQueries(MongoQueries):
    collection_name = "accounts"

    def get(self, email: str) -> AccountOutWithPassword:
        account = self.collection.find_one({"email": email})
        if account is None:
            return None
        account["id"] = str(account["_id"])
        return AccountOutWithPassword(**account)

    def create(
        self, account_in: AccountIn, hashed_password: str
    ) -> AccountOutWithPassword:
        account_in = account_in.dict()
        if self.get(account_in["email"]) is not None:
            raise DuplicateAccountError
        account_in["hashed_password"] = hashed_password
        del account_in["password"]
        self.collection.insert_one(account_in)
        account_in["id"] = str(account_in["_id"])
        return AccountOutWithPassword(**account_in)

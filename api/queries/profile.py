from queries.client import MongoQueries


class ChefQueries(MongoQueries):
    collection_name = "chefs"

    def get_credits_names_for_recipe_id(self, recipe_id: int):
        credit_names = []
        chefs = self.collection.find({"recipe_id": recipe_id})
        for chef in chefs:
            for credit in chef.get("credits", []):
                credit_names.append(credit["name"])
        return credit_names

from fastapi.testclient import TestClient
from main import app
from queries.favorites import FavoriteQueries
from models import FavoriteIn
from authenticator import authenticator

client = TestClient(app)


def fake_get_current_account_data():
    return {"id": "1000", "username": "abc@123.com"}


class FakeFavoriteQueries:
    def create(self, favorite_in: FavoriteIn, account_id: str):
        favorite = favorite_in.dict()
        favorite["id"] = "fake-id"
        favorite["account_id"] = account_id
        return favorite

    def list_all_for_account(self, account_id: str):
        return [
            {
                "id": "64b8004d75aef33d8fddd707",
                "account_id": account_id,
                "recipe_id": "5276"
            }
        ]

    def delete(self, recipe_id: str, account_id: str):
        return {"success": True}


def test_create_favorite():
    app.dependency_overrides[FavoriteQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_account_data
    favorite_in = {"recipe_id": "600"}

    res = client.post("/api/favorites", json=favorite_in)
    data = res.json()

    assert data == {
        "id": "fake-id",
        "account_id": "1000",
        "recipe_id": "600"
    }
    assert res.status_code == 200


def test_list_favorites_for_current_account():
    app.dependency_overrides[FavoriteQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_account_data

    res = client.get("/api/favorites")
    data = res.json()

    assert res.status_code == 200
    assert data == {
        "favorites": [
            {
                "id": "64b8004d75aef33d8fddd707",
                "account_id": "1000",
                "recipe_id": "5276"
            }
        ]
    }


def test_delete_favorite():
    app.dependency_overrides[FavoriteQueries] = FakeFavoriteQueries
    app.dependency_overrides[
        authenticator.get_current_account_data
        ] = fake_get_current_account_data

    res = client.delete("/api/favorites/5000")
    data = res.json()

    assert res.status_code == 200
    assert {"success": True} == data

import React, { useState, useEffect } from 'react';
import { useGetFavoritesQuery, useDeleteFavoriteMutation } from './app/apiSlice';
import RecipeCard from './RecipeCard';
import Lottie from 'lottie-react';
import animationData from './animations/Loadingdots.json';
import Favorite from './animations/favorite.json';
import './style/Favorite.css';

const FavoriteRecipes = () => {
    const [favoriteRecipes, setFavoriteRecipes] = useState([]);
    const [isLoading, setIsLoading] = useState(true);
    const [isError, setIsError] = useState(false);
    const { data: favorites } = useGetFavoritesQuery();
    const [deleteFavoriteMutation] = useDeleteFavoriteMutation();

    useEffect(() => {
        const fetchFavoriteRecipesData = async () => {
            if (favorites && favorites.length > 0) {
                try {
                    const favoriteRecipesWithDetails = [];
                    for (const favorite of favorites) {
                        const recipeDataResponse = await fetch(`http://localhost:8000/api/recipes/${favorite.recipe_id}`);
                        if (recipeDataResponse.ok) {
                            const recipeData = await recipeDataResponse.json();
                            favoriteRecipesWithDetails.push({
                                ...favorite,
                                name: recipeData.name,
                                thumbnail_url: recipeData.thumbnail_url,
                            });
                        }
                    }

                    setFavoriteRecipes(favoriteRecipesWithDetails);
                    setIsLoading(false);
                } catch (error) {
                    setIsError(true);
                    setIsLoading(false);
                }
            } else {
                setIsLoading(false);
            }
        };

        fetchFavoriteRecipesData();
    }, [favorites]);

    const handleDeleteFavorite = async (favoriteId) => {
        try {
            const { data } = await deleteFavoriteMutation(favoriteId);
            setFavoriteRecipes((prevRecipes) =>
                prevRecipes.filter((favorite) => favorite.id !== favoriteId)
            );
        } catch (error) {
            console.error('Error deleting favorite recipe:', error);
        }
    };

    if (isLoading) {
        return (
            <div className="loading-container">
                <Lottie
                    animationData={animationData}
                    loop={false}
                    autoplay={true}
                    style={{ width: '500px', height: '500px' }}

                />
            </div>
        );
    }

    if (isError) {
        return <div>Error fetching favorites</div>;
    }


    return (
        <div className="favorite-recipes">
            <div className="favorite-details">
                <h1 className="favorite-recipes-title">My Favorite Recipes</h1>
                <div className='fav-lottie-container'>
                    <Lottie animationData={Favorite} loop={true} autoplay={true} />
                </div>
            </div>
            <div className='fav-grid'>
                <div className="recipe-cards-container">
                    {favoriteRecipes && favoriteRecipes.length > 0 ? (
                        favoriteRecipes.map((favorite) => (
                            <RecipeCard
                                key={favorite.recipe_id}
                                recipeId={favorite.recipe_id}
                                thumbnailUrl={favorite.thumbnail_url}
                                recipeName={favorite.name}
                                onDelete={handleDeleteFavorite}
                            />
                        ))
                    ) : (
                        <div>No favorite recipes found.</div>
                    )}
                </div>
            </div>
        </div>
    );
};

export default FavoriteRecipes;

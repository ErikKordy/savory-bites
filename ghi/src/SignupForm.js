import { useState } from "react";
import { useSignupMutation } from "./app/apiSlice";
import { useNavigate } from "react-router-dom";
import './style/Forms.css';
import ErrorNotification from './ErrorNotification';
import validator from 'validator';
import {
    MDBContainer,
    MDBRow,
    MDBCol,
} from 'mdb-react-ui-kit';
import signupImage from './images/signup.jpg';
import './style/Forms.css';

const SignupForm = () => {
    const [signup] = useSignupMutation();
    const navigate = useNavigate();
    const [password, setPassword] = useState("");
    const [passwordConfirmation, setPasswordConfirmation] = useState("");
    const [errorMessage, setErrorMessage] = useState("");
    const [first, setFirst] = useState("");
    const [last, setLast] = useState("");
    const [email, setEmail] = useState("");
    const [emailError, setEmailError] = useState("");

    const handlePasswordChange = (e) => {
        setPassword(e.target.value);
        setErrorMessage(e.target.value !== passwordConfirmation ? "Password does not match confirmation" : "");
    };

    const handlePasswordConfirmationChange = (e) => {
        setPasswordConfirmation(e.target.value);
        setErrorMessage(e.target.value !== password ? "Password does not match confirmation" : "");
    };

    const handleEmailChange = (e) => {
        const email = e.target.value;
        const isValidEmail = validator.isEmail(email);
        setEmail(email);
        setEmailError(!isValidEmail ? "Invalid email format" : "");
    };

    const handleSubmit = async (e) => {
        e.preventDefault();
        if (password !== passwordConfirmation) {
            setErrorMessage("Password does not match confirmation");
            return;
        }

        if (emailError) {
            setErrorMessage("Please fix the errors in the form");
            return;
        }

        try {
            const response = await signup({
                email,
                password,
                first_name: first,
                last_name: last,
            });

            if (response.error) {
                if (response.error.status === 400) {
                    setErrorMessage("Email is already in use");
                } else {
                    setErrorMessage(response.error.message);
                }
                return;
            }

            navigate("/login");
        } catch (error) {
            setErrorMessage("An error occurred during signup. Please try again.");
        }
    };

    const errorStyle = {
        borderColor: errorMessage ? "red" : "green",
    };

    return (
        <div className="signup-container">
            <MDBContainer fluid>
                <MDBRow>
                    <MDBCol sm="6">
                        {errorMessage && (
                            <div className="alert alert-danger">{errorMessage}</div>
                        )}
                        <form
                            onSubmit={handleSubmit}
                            className="d-flex flex-column justify-content-center h-custom-2 w-75 pt-4 login-form"
                        >
                            <h3 className="fw-normal mb-3 ps-5 pb-3" style={{ letterSpacing: '1px' }}>
                                Sign up
                            </h3>
                            <div className="mb-4 mx-5 w-100">
                                <label className="form-label">Email address</label>
                                <input
                                    name="email"
                                    type="text"
                                    className="form-control"
                                    value={email}
                                    style={errorStyle}
                                    onChange={handleEmailChange}
                                />
                                {emailError && <ErrorNotification>{emailError}</ErrorNotification>}
                            </div>
                            <div className="mb-4 mx-5 w-100">
                                <label className="form-label">Password</label>
                                <input
                                    name="password"
                                    type="password"
                                    className="form-control"
                                    style={errorStyle}
                                    value={password}
                                    onChange={handlePasswordChange}
                                />
                            </div>
                            <div className="mb-4 mx-5 w-100">
                                <label className="form-label">Confirm Password</label>
                                <input
                                    name="password"
                                    type="password"
                                    className="form-control"
                                    style={errorStyle}
                                    value={passwordConfirmation}
                                    onChange={handlePasswordConfirmationChange}
                                />
                            </div>
                            <div className="mb-4 mx-5 w-100">
                                <label className="form-label">First Name</label>
                                <input
                                    name="first"
                                    type="text"
                                    className="form-control"
                                    value={first}
                                    onChange={(e) => {
                                        setFirst(e.target.value);
                                    }}
                                />
                            </div>
                            <div className="mb-4 mx-5 w-100">
                                <label className="form-label">Last Name</label>
                                <input
                                    name="last"
                                    type="text"
                                    className="form-control"
                                    value={last}
                                    onChange={(e) => {
                                        setLast(e.target.value);
                                    }}
                                />
                            </div>
                            <div>
                                <input className="btn btn-primary" type="submit" value="Register" />
                            </div>
                        </form>
                    </MDBCol>
                    <MDBCol sm="6" className="d-none d-sm-block px-0">
                        <img
                            src={signupImage}
                            alt="Signup image"
                            className="w-100"
                            style={{ objectFit: 'cover', objectPosition: 'right', height: '100vh', width: '100vw' }}
                        />
                    </MDBCol>
                </MDBRow>
            </MDBContainer>
        </div>
    );
};

export default SignupForm;

import { NavLink } from 'react-router-dom';
import { useGetAccountQuery, useLogoutMutation } from "./app/apiSlice";
import { useNavigate } from "react-router-dom";
import React, { useState } from 'react';
import './style/Nav.css';

function Nav() {
    const navigate = useNavigate();
    const { data: account } = useGetAccountQuery();
    const [logout] = useLogoutMutation();
    const [isDropdownOpen, setDropdownOpen] = useState(false);

    const handleDropdownToggle = () => {
        setDropdownOpen(!isDropdownOpen);
    };

    return (
        <nav className="navbar navbar-expand-lg bg-body-tertiary container-fluid">
            <div className="container-fluid">
                <NavLink className="navbar-brand" to="/">
                    <i className="bi bi-egg-fried"></i> SB
                </NavLink>
                <button className="navbar-toggler" type="button" onClick={handleDropdownToggle}>
                    <span className="navbar-toggler-icon"></span>
                </button>
                <div className={`collapse navbar-collapse ${isDropdownOpen ? 'show' : ''}`} id="navbarSupportedContent">
                    <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                        <li className="nav-item">
                            <NavLink className="nav-link active" aria-current="page" to="/">Home</NavLink>
                        </li>
                        {account && <li className="nav-item">
                            <NavLink className="nav-link" to="/favorites/mine">My Recipes</NavLink>
                        </li>}
                        {!account && <li className="nav-item">
                            <NavLink className="nav-link" to="/login">Login</NavLink>
                        </li>}
                        {!account && <li className="nav-item">
                            <NavLink className="nav-link" to="/signup">Signup</NavLink>
                        </li>}
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/faq">FAQ</NavLink>
                        </li>
                        <li className="nav-item">
                            <NavLink className="nav-link" to="/about">Contact Us</NavLink>
                        </li>
                        <li className="nav-item">
                            <button
                                className="nav-link ps-2 btn btn-link"
                                onClick={() => window.open("https://gitlab.com/salkaraisha/module3-project-gamma")}
                            >
                                <i className="bi bi-git"></i>
                            </button>
                        </li>
                    </ul>
                    {account ? (
                        <button
                            className="btn btn-outline-danger"
                            onClick={() => {
                                logout();
                                navigate("/login");
                            }}
                        >
                            <i className="bi bi-box-arrow-right"></i>
                        </button>
                    ) : null}
                </div>
            </div>
        </nav>
    )
}

export default Nav;

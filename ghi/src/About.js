import React from 'react';
import './style/About.css';
import Lottie from 'lottie-react';
import teamAnimation from './animations/team.json';


const TeamAnimation = () => {
    return (
        <div>
            <Lottie animationData={teamAnimation} />
        </div>
    );
};


export default function About() {
    return (
        <section className="team-section">
            <div className="title">
                <h1>Meet the team</h1>
            </div>
            <div className="about-lottie-container">
            <TeamAnimation />
            </div>
            <div className='about-description'>
                <p>Meet our team! We are a group of skilled and passionate full-stack developers dedicated to creating innovative solutions. Our team members bring their unique expertise and creativity to every project. With a strong foundation in development, we work together seamlessly to deliver high-quality results. Get to know our talented team members below and explore their individual profiles to learn more about their skills and contributions. Connect with us on LinkedIn and GitLab to stay updated on our latest projects and collaborations. We look forward to working with you!</p>
            </div>
            <div className="team-member-container">
                <TeamMember
                    name="Samantha Alkaraisha"
                    role="Fullstack Developer"
                    imageSrc="https://play.nintendo.com/images/Nintendo_Profile_Cat_v01.b204adaa2e16d0ba.png"
                    linkedinLink="https://www.linkedin.com/in/salkaraisha/"
                    gitlabLink="https://gitlab.com/salkaraisha"
                />
                <TeamMember
                    name="Erik Kordylasinki"
                    role="Fullstack Developer"
                    imageSrc="https://play.nintendo.com/images/Nintendo_Profile_Croc_v01.b204adaa2e16d0ba.png"
                    linkedinLink="https://www.linkedin.com/in/erik-kordylasinski/"
                    gitlabLink="https://gitlab.com/ErikKordy"
                />
                <TeamMember
                    name="Maria Perez"
                    role="Fullstack Developer"
                    imageSrc="https://play.nintendo.com/images/Nintendo_Profile_Unicorn_v01.b204adaa2e16d0ba.png"
                    linkedinLink="https://www.linkedin.com/in/maria-sofia-montaner-perez/"
                    gitlabLink="https://gitlab.com/mariaperez"
                />
                <TeamMember
                    name="Gabriel Lugo"
                    role="Fullstack Developer"
                    imageSrc="https://play.nintendo.com/images/Nintendo_Profile_Raccoon_v01.b204adaa2e16d0ba.png"
                    linkedinLink="https://www.linkedin.com/in/gabrielblugo/"
                    gitlabLink="https://gitlab.com/glugoo"
                />
            </div>
        </section>
    );
}

function TeamMember({ name, role, imageSrc, linkedinLink, gitlabLink }) {
    return (
        <div className="team-member-card">
            <img className="team-member-image" src={imageSrc} alt={name} />
            <h1 className="team-member-name">{name}</h1>
            <p className="team-member-role">{role}</p>
            <div className="team-member-links">
                <a
                    href={linkedinLink}
                    target="_blank"
                    rel="noreferrer"
                    className="team-member-link"
                    aria-label="LinkedIn"
                >
                    <img alt="" className="team-member-icon" src="https://img.icons8.com/fluency/512/linkedin.png" />
                </a>
                <a
                    href={gitlabLink}
                    target="_blank"
                    rel="noreferrer"
                    className="team-member-link"
                    aria-label="Gitlab"
                >
                    <img alt="" className="team-member-icon" src="https://img.icons8.com/color/512/gitlab.png" />
                </a>
            </div>
        </div>
    );
}

import React, { useState } from 'react';
import { useNavigate } from "react-router-dom";
import { useLoginMutation } from "./app/apiSlice";
import {
    MDBContainer,
    MDBRow,
    MDBCol,
} from 'mdb-react-ui-kit';
import loginImage from './images/login.jpg';
import './style/Forms.css';

const LoginForm = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const navigate = useNavigate();
    const [login] = useLoginMutation();
    const [errorMessage, setErrorMessage] = useState("");

    const handleSubmit = async (e) => {
        e.preventDefault();
        const response = await login({ username: email, password });
        if (response.error) {
            setErrorMessage("Account not found");
            setEmail("");
            setPassword("");
        } else {
            navigate("/");
        }
    };

    return (
        <MDBContainer fluid>
            <MDBRow>

                <MDBCol sm='6'>
                    {errorMessage && <div className="alert alert-danger">{errorMessage}</div>}
                    <form onSubmit={handleSubmit} className='d-flex flex-column justify-content-center h-custom-2 w-75 pt-4'>

                        <h3 className="fw-normal mb-3 ps-5 pb-3" style={{ letterSpacing: '1px' }}>Log in</h3>

                        <div className="mb-4 mx-5 w-100">
                            <label className="form-label">Email address</label>
                            <input
                                name="email"
                                type="email"
                                className="form-control"
                                value={email}
                                onChange={(e) => setEmail(e.target.value)}
                            />
                        </div>
                        <div className="mb-4 mx-5 w-100">
                            <label className="form-label">Password</label>
                            <input
                                name="password"
                                type="password"
                                className="form-control"
                                value={password}
                                onChange={(e) => setPassword(e.target.value)}
                            />
                        </div>

                        <div>
                            <input className="btn btn-primary" type="submit" value="Login" />
                        </div>
                        <p className='ms-5'>Don't have an account? <a href="/signup" className="link-info">Register here</a></p>

                    </form>

                </MDBCol>

                <MDBCol sm='6' className='d-none d-sm-block px-0'>
                    <img src={loginImage}
                        alt="Login image" className="w-100" style={{ objectFit: 'cover', objectPosition: 'left' }} />
                </MDBCol>

            </MDBRow>

        </MDBContainer>
    );
};

export default LoginForm;

import React, { useEffect, useState } from 'react';
import Lottie from 'lottie-react';
import loading from './animations/loading.json';

export default function Loading() {
    const [loopCount, setLoopCount] = useState(0);

    useEffect(() => {
        if (loopCount < 3) {
            const timer = setTimeout(() => {
                setLoopCount(loopCount + 1);
            }, 1000);

            return () => clearTimeout(timer);
        }
    }, [loopCount]);

    useEffect(() => {
        if (loopCount === 3) {
        }
    }, [loopCount]);

    return (
        <div className="main-loading-container">
            <Lottie
                animationData={loading}
                loop={false}
                style={{ width: '500px', height: '500px' }}
                onComplete={() => setLoopCount(loopCount + 1)}
                animationOptions={{
                    loop: true,
                    autoplay: true,
                    animationData: loading,
                    rendererSettings: {
                        preserveAspectRatio: 'xMidYMid slice',
                    },
                }}
            />
        </div>
    );
}

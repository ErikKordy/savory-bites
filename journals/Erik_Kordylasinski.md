6/26/23 - Worked on creating the docker files for the work enviornment, which also included incorporating mongo. Then started implementing authentication, specifically to have the login and logout endpoints show up on the fastAPI side. Blocker currently is how to create our functions.

6/27/23 - Started working on the functions to handle the create and get for the login information. Once that was completed, followed the rest of the videos to implement the username into the create function to make the enpoint work properly after re-watching curtis's video on auth. Completed everything, No blockers today!

6/28/23 - I worked on getting the data from 3rd party API(tasty) to show up on the fastAPI side. Currently my blocker is trying to figure out how to add my rapidAPI key to properly show the link & pull the data.
UPDATE: Worked on it further after reading the docs & realizing i didnt have python set for requests. Got the List & detail working now.

6/29/23 - After finishing up the api data retrieval yesterday, i worked on creating favorites for a user and being able to delete the favorites. No blockers today, was able to finish.

7/10/23 - Worked on getting all the data from the API for our homepage as a team. Was able create cards and have them set-up in a grid with most of the information we wanted to add there. Styled the nav bar & worked on having the routes work. Had blockers trying to finish the grid but got it figured out.

7/11/23 - Spend most of the day trying to figure out how to get the signup form & login to work. Ended up using redux to figure it out. Had a blocker because we have email as our sign up & login input but was able to figure out how to incorporate that.

7/12-7/17 -Throughout this time I worked on getting a toggle for the darkmode. It was working but then we ran into issues when we had a lot of changes pushed to main that took a day to debug. Since then I've been working on getting the favorites data to show the image and name of the recipe, which i was stuck on for awhile but finally figured out. Still need to fix a couple bugs and add the delete function.

7/18- 7/20 - Got the delete function workin in the favorite recipes Lists. Created 3 tests for the favorite routes. Created a carousel using bootstrap for the featured recipes on the home page. Also added some error handling for the sign-up page.

7/24-27 - Worked on getting little things done like adding a message that pops up when something is favorited or removed from favorites. Added a confirmation window before deleting a favorite. Added some css & worked on the readME
